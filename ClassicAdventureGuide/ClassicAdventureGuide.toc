## Interface: 20501
## Title: Classic Adventure Guide
## Notes: Extensive Adventure Guide for WoW Classic
## Author: Wildmane-Venoxis
## Version: 0.1
## DefaultState: Enabled
## SavedVariables: ClassicAdventureGuideDB
## OptionalDeps: Ace3, LibDataBroker-1.1

#@no-lib-strip@
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
libs\LibDBIcon-1.0\LibDBIcon-1.0.lua

libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceDB-3.0\AceDB-3.0.xml
libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
libs\AceEvent-3.0\AceEvent-3.0.xml
libs\AceConsole-3.0\AceConsole-3.0.xml
libs\AceHook-3.0\AceHook-3.0.xml
libs\AceLocale-3.0\AceLocale-3.0.xml
libs\AceGUI-3.0\AceGUI-3.0.xml
libs\AceConfig-3.0\AceConfig-3.0.xml

libs\LibLootTable-1.0\LibLootTable-1.0.lua
#@end-no-lib-strip@

data\Dungeons.lua
data\Raids.lua
data\WorldBosses.lua
data\QuestingZones.lua

# encounters for each dungeon
data\encounters\rfc.lua
data\encounters\bfd.lua
data\encounters\brd.lua
data\encounters\deadm.lua
data\encounters\dmeast.lua
data\encounters\dmwest.lua
data\encounters\dmnorth.lua
data\encounters\gnome.lua
data\encounters\lbrs.lua
data\encounters\mara.lua
data\encounters\rfd.lua
data\encounters\rfk.lua
data\encounters\scholo.lua
data\encounters\sfk.lua
data\encounters\smarmory.lua
data\encounters\smcath.lua
data\encounters\smgrave.lua
data\encounters\smlib.lua
data\encounters\stock.lua
data\encounters\strat.lua
data\encounters\temple.lua
data\encounters\ubrs.lua
data\encounters\ulda.lua
data\encounters\wc.lua
data\encounters\zf.lua

# encounters for each raid
data\encounters\mc.lua
data\encounters\ony.lua
data\encounters\bwl.lua
data\encounters\zg.lua
data\encounters\aq20.lua
data\encounters\aq40.lua
data\encounters\naxx.lua

Setup.lua
Utilities.lua
ClassicAdventureGuide.lua
