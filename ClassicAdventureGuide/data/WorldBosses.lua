CAG_Worldbosses = {
	{
		short = "azu",
		name = "Azuregos",
		zone = "Azshara",
		numPlayers = 40,
		way_x = 48.85,
		way_y = 80.40,
		way_map = 1447
	},
	{
		short = "kazz",
		name = "Lord Kazzak",
		zone = "Blasted Lands",
		numPlayers = 40,
		way_x = 43.65,
		way_y = 76.45,
		way_map = 1419
	},
	{
		short = "leth",
		name = "Lethon (Dragons of Nightmare)",
		zone = "Duskwood / Hinterlands / Feralas / Ashenvale",
		numPlayers = 40
	},
	{
		short = "emer",
		name = "Emeriss (Dragons of Nightmare)",
		zone = "Duskwood / Hinterlands / Feralas / Ashenvale",
		numPlayers = 40
	},
	{
		short = "emer",
		name = "Ysondre (Dragons of Nightmare)",
		zone = "Duskwood / Hinterlands / Feralas / Ashenvale",
		numPlayers = 40
	},
	{
		short = "taer",
		name = "Taerar (Dragons of Nightmare)",
		zone = "Duskwood / Hinterlands / Feralas / Ashenvale",
		numPlayers = 40
	}
}