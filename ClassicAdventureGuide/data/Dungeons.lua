CAG_Dungeons = {
	{
		short = "rfc",
		name = "Ragefire Chasm",
		zone = "Orgrimmar",
		mapId = "CL_RagefireChasm",
		minLevel = 13,
		maxLevel = 18,
		faction = "Horde",
		way_x = 51.00,
		way_y = 49.55,
		way_map = 1454
	},
	{
		short = "wc",
		name = "Wailing Caverns",
		zone = "The Barrens",
		mapId = "CL_WailingCaverns",
		mapIdEntrance = "CL_WailingCavernsEnt",
		minLevel = 15,
		maxLevel = 25,
		way_x = 46.10,
		way_y = 36.15,
		way_map = 1413
	},
	{
		short = "deadm",
		name = "The Deadmines",
		zone = "Westfall",
		mapId = "CL_TheDeadmines",
		mapIdEntrance = "CL_TheDeadminesEnt",
		minLevel = 18,
		maxLevel = 23,
		way_x = 42.95,
		way_y = 71.50,
		way_map = 1436
	},
	{
		short = "sfk",
		name = "Shadowfang Keep",
		zone = "Silverpine Forest",
		mapId = "CL_ShadowfangKeep",
		minLevel = 13,
		maxLevel = 18,
		way_x = 43.65,
		way_y = 68.15,
		way_map = 1421
	},
	{
		short = "stock",
		name = "The Stockade",
		zone = "Stormwind",
		mapId = "CL_TheStockade",
		minLevel = 22,
		maxLevel = 30,
		faction = "Alliance",
		way_x = 42.55,
		way_y = 59.40,
		way_map = 1453
	},
	{
		short = "bfd",
		name = "Blackfathom Deeps",
		zone = "Ashenvale",
		mapId = "CL_BlackfathomDeepsA",
		mapIdEntrance = "CL_BlackfathomDeepsEnt",
		minLevel = 24,
		maxLevel = 32,
		way_x = 14.90,
		way_y = 15.70,
		way_map = 1440
	},
	{
		short = "gnome",
		name = "Gnomeregan",
		zone = "Dun Morogh",
		mapId = "CL_Gnomeregan",
		mapIdEntrance = "CL_GnomereganEnt",
		minLevel = 29,
		maxLevel = 38,
		way_x = 24.85,
		way_y = 39.85,
		way_map = 1426
	},
	{
		short = "rfk",
		name = "Razorfen Kraul",
		zone = "The Barrens",
		mapId = "CL_RazorfenKraul",
		minLevel = 30,
		maxLevel = 40,
		way_x = 42.00,
		way_y = 90.00,
		way_map = 1413
	},
	{
		short = "smgrave",
		name = "Scarlet Monastery: Graveyard",
		zone = "Tirisfal Glades",
		mapId = "CL_SMGraveyard",
		mapIdEntrance = "CL_ScarletMonasteryEnt",
		minLevel = 28,
		maxLevel = 38,
		way_x = 83.00,
		way_y = 34.65,
		way_map = 1420
	},
	{
		short = "smlib",
		name = "Scarlet Monastery: Library",
		zone = "Tirisfal Glades",
		mapId = "CL_SMLibrary",
		mapIdEntrance = "CL_ScarletMonasteryEnt",
		minLevel = 29,
		maxLevel = 39,
		way_x = 83.00,
		way_y = 34.65,
		way_map = 1420
	},
	{
		short = "smarmory",
		name = "Scarlet Monastery: Armory",
		zone = "Tirisfal Glades",
		mapId = "CL_SMArmory",
		mapIdEntrance = "CL_ScarletMonasteryEnt",
		minLevel = 32,
		maxLevel = 42,
		way_x = 83.00,
		way_y = 34.65,
		way_map = 1420
	},
	{
		short = "smcath",
		name = "Scarlet Monastery: Cathedral",
		zone = "Tirisfal Glades",
		mapId = "CL_SMCathedral",
		mapIdEntrance = "CL_ScarletMonasteryEnt",
		minLevel = 35,
		maxLevel = 45,
		way_x = 83.00,
		way_y = 34.65,
		way_map = 1420
	},
	{
		short = "rfd",
		name = "Razorfen Downs",
		zone = "The Barrens",
		mapId = "CL_RazorfenDowns",
		minLevel = 40,
		maxLevel = 50,
		way_x = 50.00,
		way_y = 92.50,
		way_map = 1413
	},
	{
		short = "ulda",
		name = "Uldaman",
		zone = "Badlands",
		mapId = "CL_Uldaman",
		mapIdEntrance = "CL_UldamanEnt",
		minLevel = 42,
		maxLevel = 52,
		way_x = 43.55,
		way_y = 14.25,
		way_map = 1418
	},
	{
		short = "zf",
		name = "Zul'Farrak",
		zone = "Tanaris",
		mapId = "CL_ZulFarrak",
		minLevel = 44,
		maxLevel = 54,
		way_x = 39.00,
		way_y = 17.85,
		way_map = 1446
	},
	{
		short = "mara",
		name = "Maraudon",
		zone = "Desolace",
		mapId = "CL_Maraudon",
		mapIdEntrance = "CL_MaraudonEnt",
		minLevel = 45,
		maxLevel = 57,
		way_x = 29.80,
		way_y = 63.25,
		way_map = 1443
	},
	{
		short = "temple",
		name = "The Temple of Atal'Hakkar",
		zone = "Swamp of Sorrows",
		mapId = "CL_TheSunkenTemple",
		mapIdEntrance = "CL_MaraudonEnt",
		minLevel = 50,
		maxLevel = 60,
		way_x = 70.20,
		way_y = 54.95,
		way_map = 1435
	},
	{
		short = "brd",
		name = "Blackrock Depths",
		zone = "Burning Steppes",
		mapId = "CL_BlackrockDepths",
		mapIdEntrance = "CL_BlackrockMountainEnt",
		minLevel = 52,
		maxLevel = 60,
		way_x = 29.50,
		way_y = 37.75,
		way_map = 1428
	},
	{
		short = "lbrs",
		name = "Lower Blackrock Spire",
		zone = "Burning Steppes",
		mapId = "CL_BlackrockSpireLower",
		mapIdEntrance = "CL_BlackrockMountainEnt",
		minLevel = 55,
		maxLevel = 60,
		way_x = 29.50,
		way_y = 37.75,
		way_map = 1428
	},
	{
		short = "ubrs",
		name = "Upper Blackrock Spire",
		zone = "Burning Steppes",
		mapId = "CL_BlackrockSpireUpper",
		mapIdEntrance = "CL_BlackrockMountainEnt",
		minLevel = 58,
		maxLevel = 60,
		way_x = 29.50,
		way_y = 37.75,
		way_map = 1428
	},
	{
		short = "scholo",
		name = "Scholomance",
		zone = "Western Plaguelands",
		mapId = "CL_Scholomance",
		minLevel = 58,
		maxLevel = 60,
		way_x = 69.95,
		way_y = 74.00,
		way_map = 1422
	},
	{
		short = "strat",
		name = "Stratholme",
		zone = "Eastern Plaguelands",
		mapId = "CL_Stratholme",
		minLevel = 58,
		maxLevel = 60,
		way_x = 30.85,
		way_y = 12.95,
		way_map = 1423
	},
	{
		short = "dmeast",
		name = "Dire Maul (east)",
		zone = "Feralas",
		mapId = "CL_DireMaulEast",
		mapIdEntrance = "CL_DireMaulEnt",
		minLevel = 58,
		maxLevel = 60,
		way_x = 1111,
		way_y = 11111,
		way_map = 1444
	},
	{
		short = "dmwest",
		name = "Dire Maul (west)",
		zone = "Feralas",
		mapId = "CL_DireMaulWest",
		mapIdEntrance = "CL_DireMaulEnt",
		minLevel = 58,
		maxLevel = 60,
		way_x = 1111,
		way_y = 11111,
		way_map = 1444
	},
	{
		short = "dmnorth",
		name = "Dire Maul (north)",
		zone = "Feralas",
		mapId = "CL_DireMaulNorth",
		mapIdEntrance = "CL_DireMaulEnt",
		minLevel = 58,
		maxLevel = 60,
		way_x = 59.35,
		way_y = 42.60,
		way_map = 1444
	}
}