CAG_Raids = {
	{
		short = "mc",
		name = "Molten Core",
		zone = "Burning Steppes",
		mapId = "CL_MoltenCore",
		mapIdEntrance = "CL_BlackrockMountainEnt",
		numPlayers = 40,
		way_x = 29.50,
		way_y = 37.75,
		way_map = 1428
	},
	{
		short = "ony",
		name = "Onyxia's Lair",
		zone = "Dustwallow Marsh",
		mapId = "CL_OnyxiasLair",
		numPlayers = 40,
		way_x = 53.20,
		way_y = 76.30,
		way_map = 1445
	},
	{
		short = "bwl",
		name = "Blackwing Lair",
		zone = "Burning Steppes",
		mapId = "CL_BlackwingLair",
		mapIdEntrance = "CL_BlackrockMountainEnt",
		numPlayers = 40,
		way_x = 29.50,
		way_y = 37.75,
		way_map = 1428
	},
	{
		short = "zg",
		name = "Zul'Gurub",
		zone = "Stranglethorn Vale",
		mapId = "CL_ZulGurub",
		numPlayers = 20,
		way_x = 51.00,
		way_y = 17.40,
		way_map = 1434
	},
	{
		short = "aq20",
		name = "Ruins of Ahn'Qiraj",
		zone = "Silithus",
		mapId = "CL_TheRuinsofAhnQiraj",
		numPlayers = 20,
		way_x = 29.50,
		way_y = 94.50,
		way_map = 1451
	},
	{
		short = "aq40",
		name = "Temple of Ahn'Qiraj",
		zone = "Silithus",
		mapId = "CL_TheTempleofAhnQiraj",
		numPlayers = 40,
		way_x = 29.50,
		way_y = 94.50,
		way_map = 1451
	},
	{
		short = "naxx",
		name = "Naxxramas",
		zone = "Eastern Plaguelands",
		mapId = "CL_Naxxramas",
		numPlayers = 40,
		way_x = 39.65,
		way_y = 26.35,
		way_map = 1423
	}
}