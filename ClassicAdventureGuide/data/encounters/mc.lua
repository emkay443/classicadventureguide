CAG_Encounters_mc = {
	{
		name = "Lucifron",
		npc_id = 12118,
		phases = {
			{
				strategy = {
					"Clear the area of trash mobs so that ranged DPS and healers have space to stay far away from Lucifron.",
					"@priest Buff all players, especially tank and melee DPS, with @spell[10958][Shadow Protection].",
					"@tank@melee TIP: If you take too much damage from Shadow Shock, you may use Shadow resistance gear and/or (Greater) Shadow Protection Potions to stay alive. This is not usually required."
				},
				preparation = true

			},
			{
				abilities = { 19460, 19703, 19702, 20604, 20605 },
				-- Shadow Shock, Lucifron's Curse, Impending Doom, Dominate Mind, Cleave
				strategy = {
					"Make sure you do not accidentally kill players who are mind-controlled by @spell[20604][Dominate Mind].",
					" ",
					"@tank Off-tanks should pick up the two Flamewaker Protectors immediately and have them face away from the raid to avoid their @spell[20605][Cleave], but close to Lucifron so that melee DPS can cleave the mobs.",
					"@heal Do not cast any spells while cursed, unless absolutely necessary. Use lower rank heals to save mana.",
					"@heal@ranged Keep your distance from Lucifron to stay out of @spell[19460][Shadow Shock] range.",
					"@melee@ranged Kill the Flamewaker Protectors first. Do not kill mind-controlled players, stop cleave damage if a melee gets mind-controlled.",
					" ",
					"@druid@mage Dispel @spell[19703][Lucifron's Curse]. Decurse yourself first, then prioritize healers and tanks. @magetexts will spend a large portion of the fight decursing instead of dealing damage.",
					"@paladin@priest Dispel @spell[19702][Impending Doom]. Prioritize healers, tanks, and mind-controlled players.",
					"@paladin@priest Dispel players who are mind controlled by @spell[20604][Dominate Mind] immediately."
				}
			}
		}
	},
	{
		name = "Magmadar",
		npc_id = 11982,
		phases = {
			{
				strategy = {
					"Clear all Core Hound packs before the encounter.",
					"You should have at least one @huntertext who has learned @spell[19801][Tranquilizing Shot] in your raid. The spell book is obtained from the previous boss, Lucifron.",
					" ",
					"@alliancetext: You should have at least one Dwarf @priesttext in your raid to make use of @spell[6346][Fear Ward].",
					"@hordetext: Keep a @shamantext in the same group as the main tank to make use of @spell[8143][Tremor Totem]."
				},
				preparation = true
			},
			{
				abilities = { 19451, 19411, 19408, 19450 },
				--19451 Frenzy, 19411 Lava Bomb, 19408 Panic, 19450 Magma Spit
				strategy = {
					"Run out of the fire left by @spell[19411][Lava Bomb] as quickly as possible, it deals high damage.",
					"@tank Do not move the boss, unless you need to run out of @spell[19411][Lava Bomb].",
					"@heal Pay attention to the health of melee players, as they take unusually high damage throughout the fight due to @spell[19450][Magma Spit].",
					" ",
					"@hunter Use @spell[19801][Tranquilizing Shot] to dispel @spell[19451][Frenzy] immediately. Frenzy causes tanks and melee players to take extremely high damage.",
					"@shaman Always keep @spell[8143][Tremor Totem] up if you are in a melee group.",
					"@shaman Always keep @spell[10538][Fire Resistance Totem] up.",
					"@paladin Always keep @spell[19900][Fire Resistance Aura] up.",
					"@priest (Dwarf) Use @spell[6346][Fear Ward] to make melee players immune to @spell[19408][Panic]. Prioritize the main tank.",
					"@warrior Use @spell[18499][Berserker Rage] to gain immunity to @spell[19408][Panic].",
					" ",
					"@horde Undead players can use @spell[7744][Will of the Forsaken] to gain immunity to @spell[19408][Panic]."
				}
			}
		}
	},
	{
		name = "Gehennas",
		npc_id = 12259,
		phases = {
			{
				strategy = {
					"@melee@tank All melee players should prepare a few Free Action Potions to avoid the stun from @spell[20277][Fist of Ragnaros]."
				},
				preparation = true
			},
			{
				abilities = { 19717, 19716, 19728, 20277 },
				--19717 Rain of Fire, 19716 Gehennas' Curse, 19728 Shadow Bolt; 20277 Fist of Rag
				strategy = {
					"Run out of @spell[19717][Rain of Fire].",
					" ",
					"@melee@ranged Kill the Flamewaker Elites first.",
					"@melee@tank Keep the buff from Free Action Potion up if the Flamewakers are alive.",
					" ",
					"@druid@mage Remove @spell[19716][Gehennas' Curse]. Prioritize tanks and healers. @magetexts will spend a large portion of the fight decursing instead of dealing damage.",
					" ",
					"TIP: Optionally, you can use Restorative Potions to decurse yourself every 5 seconds, for 30 seconds. This will allow @magetexts to deal more damage and speed up the encounter."
				}
			}
		}
	},
	{
		name = "Garr",
		npc_id = 12057,
		phases = {
			{
				strategy = {
					"The general strategy for this encounter is to kill Garr first while keeping all Firesworn crowd controlled (via @spell[710][Banish]) or off-tanked.",
					"Try to have as many @warlock Warlocks as you can get. @spell[710][Banish] makes them invaluable in this fight.",
					"Give every Firesworn a raid marker icon to help tell them apart."
				},
				preparation = true
			},
			{
				abilities = { 19492, 19496, 23492 },
				-- Antimagic Pulse, Magma Shackles, Anxiety
				strategy = {
					"Kill Garr.",
					"Do not renew buffs dispelled by @spell[19492][Antimagic Pulse]. Garr will constantly use this ability, so do not waste your mana.",
					" ",
					"@spell[23492][Separation Anxiety] causes Firesworn to deal 300% increased damage when too far away from Garr.",
					"@tank Off-tanks should keep Firesworn close to Garr to avoid @spell[23492][Separation Anxiety], but make sure they don't die close to other players!",
					"@ranged@melee@heal Stay away from Firesworn.",
					" ",
					"@warlock Keep your assigned Firesworn @spell[710][Banish]ed."
				}
			},
			{
				abilities = { 20294, 19497 },
				-- Immolate, Eruption
				strategy = {
					"@spell[19497][Eruption] causes Firesworn to explode upon death, knocking close-by players far away.",
					"@melee@ranged Kill Firesworn. Stay away from those that are close to death so you don't get thrown into a mob group.",
					"@tank Tank Firesworn with your back towards the wall so you get thrown into the wall when they die."
				}
			}
		}
	},
	{
		name = "Baron Geddon",
		npc_id = 12056,
		phases = {
			{
				strategy = {
					"Clear all trash mobs by pulling them towards the entrance of the large cave where Geddon patrols, making sure to not accidentally pull the boss.",
					" ",
					"@melee@tank Fire Resistance gear or consumables, as well as (Greater) Fire Protection Potions, can be very useful for all melee players.",
					"@ranged@heal Use talents which increase your range. Try to exceed 40 yd."
				},
				preparation = true
			},
			{
				abilities = { 19659, 19695, 20475, 20478 },
				--Ignite Mana, Inferno, Living Bomb, Arma-Geddon LMAO GET IT? BECAUSE GEDDON HAHAHA - yeah I'll see myself out, thanks.
				strategy = {
					"Baron Geddon cannot be taunted. All players should be careful to not pull aggro.",
					"Run out of the raid if you become a @spell[20475][Living Bomb]. If you're facing into the large cave, run towards the left wall.",
					"Shortly before dying, Baron Geddon begins to cast @spell[20478][Armageddon]. Make sure to bring him down before he finishes casting it!",
					" ",
					"@melee@tank Immediately run away from Baron Geddon if he casts @spell[19695][Inferno]. This includes the main tank.",
					"@ranged@heal Stay at maximum range to the boss if possible. If you can stay further than 40 yd, you will not be hit by @spell[19659][Ignite Mana].",
					"@heal Place Heal over Time effects and damage shields on players who have become a @spell[20475][Living Bomb], and heal them fully before they explode.",
					" ",
					"@paladin@priest Dispel @spell[19659][Ignite Mana]. Prioritize healers; do not dispel @roguetexts or @warriortexts, as they have no mana.",
					"@mage Use @spell[11958][Ice Block] to cancel out @spell[20475][Living Bomb] damage.",
					"@paladin Use @spell[1020][Divine Shield] to cancel out @spell[20475][Living Bomb] damage."
				}
			}
		}
	},
	{
		name = "Shazzrah",
		npc_id = 12264,
		phases = {
			{
				strategy = {
					"All melee players should bring several Greater Arcane Protection Potions to survive Shazzrah's @spell[19712][Arcane Explosion].",
					"You can get away with a few melee DPS players not dealing any significant damage, but the main tank must have enough potions to last through the entire fight.",
					"Clear out the space around Shazzrah; this fight will require a lot of space.",
				},
				preparation = true
			},
			{
				abilities = { 19713, 19712, 19715, 19714 },
				--Shaz Curse, AE, Blink, Counterspell, Deaden Magic
				strategy = {
					"@melee Keep up your Greater Arcane Protection Potion damage shield.",
					"@melee@ranged If Shazzrah @spell[19712][Blink]s, he will clear all threat - immediately stop dealing damage and let the tank reestablish threat.",
					"@heal@ranged Always stay out of Shazzrah's @spell[19712][Arcane Explosion] range.",
					"@heal@ranged Make sure you are not casting a spell when Shazzrah uses @spell[19715][Counterspell]. Make use of instant cast spells.",
					" ",
					"@druid@mage Dispel @spell[19713][Shazzrah's Curse]. Prioritize the tank and melee DPS. @magetexts will spend most of the fight decursing instead of dealing damage.",
					"@mage Keep up @spell[2855][Detect Magic] on Shazzrah so that @spell[19714][Deaden Magic] can be discovered.",
					"@priest@shaman Dispel/Purge @spell[19714][Deaden Magic]."
				}
			}
		}
	},
	{
		name = "Golemagg the Incinerator",
		npc_id = 11988,
		phases = {
			{
				strategy = {
					"@tank This fight requires two main tanks and two off-tanks.",
					"@tank@melee Fire Resistance equipment and/or consumables are very useful for all melee players as they reduce @spell[13880][Magma Splash] damage."
				},
				preparation = true
			},
			{
				abilities = { 13880, 20228, 19798, 20553, 19820 },
				-- magma splash, pyro, earthquake; core rager: Golemagg's Trust, mangle
				strategy = {
					"@tank Off-tanks must position the two Core Ragers far away from Golemagg so that they will not be affected by @spell[20553][Golemagg's Trust].",
					" ",
					"Golemagg will apply @spell[13880][Magma Splash] to all melee attackers. This is a stacking debuff which deals Fire damage over time and reduces armor.",
					"@tank As soon as you have 5-6 stacks of @spell[13880][Magma Splash], stop attacking Golemagg and have the other main tank take over until the debuff wears off.",
					"@melee Keep an eye on your @spell[13880][Magma Splash] stacks. Stop attacking until the debuff wears off when you begin taking too much damage.",
					" ",
					"@heal Golemagg will target random players with @spell[20228][Pyroblast]. Pay special attention to those who get hit by it.",
					"@melee@ranged At 10% remaining health, Golemagg will begin using @spell[19798][Earthquake], which damages all players. Continue dealing as much damage to Golemagg as possible to push through before healers get overwhelmed."
				}
			}
		}
	},
	{
		name = "Sulfuron Harbinger",
		npc_id = 12098,
		phases = {
			{
				strategy = {
					"soos"
				},
				preparation = true
			},
			{
				abilities = { },
				strategy = {
					"saas"
				}
			}
		}
	},
	{
		name = "Majordomo Executus",
		npc_id = 12018,
		phases = {
			{
				strategy = {
					"soos"
				},
				preparation = true
			},
			{
				abilities = { },
				strategy = {
					"saas"
				}
			}
		}
	},
	{
		name = "Ragnaros",
		npc_id = 11502,
		phases = {
			{
				strategy = {
					"soos"
				},
				preparation = true
			},
			{
				abilities = { },
				strategy = {
					"saas"
				}
			}
		}
	}
}
