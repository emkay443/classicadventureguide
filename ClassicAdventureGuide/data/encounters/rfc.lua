CAG_Encounters_rfc = {
	{
		name = "Oggleflint",
		npc_id = 11517,
		phases = {
			{
				abilities = { 15496 },
				strategy = {
					"@ranged Crowd control one of Oggleflint's bodyguards.",
					"@tank Take the remaining add and the boss and tank them facing away from the group.",
					"@melee@tank@ranged Kill the uncontrolled add, then kill the boss.",
					"Watch out for Oggleflint's @spell[15496][Cleave] ability."
				},
				preparation = true
			}
		}
	},
	{
		name = "Taragaman the Hungerer",
		npc_id = 11520,
		phases = {
			{
				abilities = { 11970, 18072 },
				strategy = {
					"@tank Tank Taragaman away from the edges so you won't get knocked off the platform by his @spell[18072][Uppercut] ability.",
					"@melee@heal Keep max distance to Taragaman so @spell[11970][Fire Nova] doesn't hit you."
				}
			}
		}
	},
	{
		name = "Jergosh the Invoker",
		npc_id = 11518,
		phases = {
			{
				abilities = { 18267, 20800 },
				strategy = {
					"@ranged Crowd control one of Jergosh's bodyguards.",
					"@melee@ranged Kill the uncontrolled add, then kill the boss.",
					"@heal If you can, remove @spell[18267][Curse of Weakness] from party members."
				}
			}
		}
	},
	{
		name = "Bazzalan",
		npc_id = 11519,
		phases = {
			{
				abilities = { 14873, 2818 },
				strategy = {
					"The add to Bazzalan's left can be pulled without pulling Bazzalan. Kill it first.",
					"@ranged Crowd control the add to Bazzalan's right.",
					"@melee@ranged Focus on Bazzalan, then kill the remaining add."
				}
			}
		}
	}
}