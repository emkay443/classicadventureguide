CAG_Encounters_wc = {
	{
		name = "Kresh",
		npc_id = 3653,
		description = "Kresh is a level 20 elite turtle located in the river of the Wailing Caverns. He is one of the first bosses of the dungeon.",
		phases = {
			{
				abilities = { },
				strategy = {
					"Nothing special. Do damage to him, kill him."
				}
			}
		}
	},
	{
		name = "Lady Anacondra",
		npc_id = 3671,
		description = "Lady Anacondra is a level 20 elite Druid of the Fang located on the cliff overlooking the Screaming Gully. She can spawn in several different locations around the cliff.",
		phases = {
			{
				abilities = { 5187, 9532, 700, 8148 },
				strategy = {
					"@melee@ranged Watch out for her @spell[700][Sleep] ability. If you can interrupt it, do that immediately.",
					"Try not to pull other Druids during the fight, as they will make the encounter more difficult."
				}
			}
		}
	},
	{
		name = "Lord Cobrahn",
		npc_id = 3669,
		description = "Lord Cobrahn is a level 20 elite Druid of the Fang located at the end of the Pit of Fangs in the Wailing Caverns.",
		phases = {
			{
				abilities = { 5187, 9532, 8040, 7965 },
				strategy = {
					"@melee@ranged Kill the 3 Deviate Pythons surrounding Cobrahn first, then switch to Cobrahn.",
					"@melee@ranged Interrupt his @spell[8040][Druid's Slumber] ability if you can."
				}
			}
		}
	},
	{
		name = "Deviate Faerie Dragon",
		npc_id = 5912,
		description = "Deviate Faerie Dragon is a level 20 rare-elite Faerie Dragon located in the Winding Chasm.",
		phases = {
			{
				abilities = { },
				strategy = {
					"@melee@ranged Crowd control one of the Druids of the Fang, kill the other and interrupt its @spell[8040][Druid's Slumber] ability. Then kill the remaining adds and focus the Faerie Dragon.",
					"@tank Keep threat on all targets. Try to keep alive and use defensive cooldowns if needed.",
					"@heal Keep your tank alive. They'll be taking a lot of damage."
				}
			}
		}
	},
	{
		name = "Lord Pythas",
		npc_id = 3670,
		description = "Lord Pythas is a level 21 elite Druid of the Fang located in the Winding Chasm.",
		phases = {
			{
				abilities = { 5187, 9532, 700, 8147 },
				strategy = {
					"@melee@ranged Crowd control the Druid of the Fang, kill the Deviate Shambler and then switch to Pythas. Watch out for his @spell[700][Sleep] ability and interrupt it if possible."
				}
			}
		}
	},
	{
		name = "Skum",
		npc_id = 3674,
		description = "Skum is a level 21 elite Thunder Lizard located in the Winding Chasm.",
		phases = {
			{
				abilities = { 6254 },
				strategy = {
					"@melee Spread out around the boss so his @spell[3674][Chained Bolt] ability can't jump to other players."
				}
			}
		}
	},
	{
		name = "Lord Serpentis",
		npc_id = 3673,
		description = "Lord Serpentis is the level 21 Druid of the Fang located at the end of the Winding Chasm.",
		phases = {
			{
				abilities = { 6778, 9532, 700 },
				strategy = {
					"@melee@ranged Watch out for his @spell[700][Sleep] ability. If you can interrupt it, do that immediately.",
				}
			}
		}
	},
	{
		name = "Verdan the Everliving",
		npc_id = 5775,
		description = "Verdan the Everliving is a level 21 elite Elemental located at the end of the Winding Chasm. He guards the waterfall shortcut just after the Lord Serptenis encounter.",
		phases = {
			{
				abilities = { 8142 },
				strategy = {
					"Nothing special. Do damage to him, kill him.",
					"@heal Keep your group alive. Watch out for Verdan's @spell[8142][Grasping Vines] ability, as it will damage all players in a 10 yard radius."
				}
			}
		}
	},
	{
		name = "Mutanus the Devourer",
		npc_id = 3654,
		description = "The Naralex Event is an optional dungeon event that unlocks when all 4 Lords of the Fang have been slain.\
\
During the event, players will be required to escort the Disciple of Naralex to the ritual stone where Naralex sleeps.\
From there, the Disciple will summon Mutanus the Devourer, a level 22 Elite Murloc Boss, whose aim is to kill the Disciple. Players must protect the Disciple at all costs while remaining alive.\
\
If the Disciple is killed or the group wipes, the entire event will fail. The event cannot be repeated until the instance is reset and all Lords of the Fang are killed again.",
		phases = {
			{
				abilities = { 8150, 7399, 7967 },
				strategy = {
					"Escort the Disciple of Naralex from the entrance of the dungeon to the ritual stone.",
					"Stay close to him as he will spawn and pull mobs along the way.",
					"Protect the Disciple of Naralex from Mutanus.",
					"If he dies or you wipe, the event will be over and cannot be repeated without resetting the whole instance!",
					" ",
					"@melee@ranged Kill the adds that spawn around the room. They will target the Disciple.",
					"@melee Watch out for Mutanus' @spell[8150][Thundercrack] ability.",
					"@heal@ranged Stay at maximum range to Mutanus.",
					"@heal Keep your group alive."
				}
			}
		}
	}
}
