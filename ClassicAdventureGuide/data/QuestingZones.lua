CAG_QuestingZones = {
	{
		name = "Durotar",
		minLevel = 1,
		maxLevel = 10,
		faction = "Horde",
		continent = 2,
		flightMasters = {
			{
				faction = "Horde",
				name = "Doras",
				location = "Orgrimmar",
				way_x = 45.0,
				way_y = 63.6,
				way_map = 1454
			}
		}
	},
	{
		name = "Mulgore",
		minLevel = 1,
		maxLevel = 10,
		faction = "Horde",
		continent = 2,
		flightMasters = {
			{
				faction = "Horde",
				name = "Tal",
				location = "Thunder Bluff",
				way_x = 47.1,
				way_y = 49.2,
				way_map = 1456
			}
		}
	},
	{
		name = "Tirisfal Glades",
		minLevel = 1,
		maxLevel = 10,
		faction = "Horde",
		continent = 1,
		flightMasters = {
			{
				faction = "Horde",
				name = "Michael Garrett",
				location = "Undercity",
				way_x = 60.7,
				way_y = 51.5,
				way_map = 1458
			}
		}
	},
	{
		name = "Elwynn Forest",
		minLevel = 1,
		maxLevel = 10,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Dungar Longdrink",
				location = "Stormwind City",
				way_x = 67.8,
				way_y = 61.35,
				way_map = 1453
			}
		}
	},
	{
		name = "Dun Morogh",
		minLevel = 1,
		maxLevel = 10,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Gryth Thurden",
				location = "Ironforge",
				way_x = 55.35,
				way_y = 47.6,
				way_map = 1455
			}
		}
	},
	{
		name = "Teldrassil",
		minLevel = 1,
		maxLevel = 10,
		faction = "Alliance",
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Vesprystus",
				location = "Rut'theran Village",
				way_x = 58.0,
				way_y = 93.0,
				way_map = 1438
			}
		}
	},
	{
		name = "The Barrens",
		minLevel = 10,
		maxLevel = 25,
		continent = 2,
		flightMasters = {
			{
				faction = "Horde",
				name = "Tal",
				location = "Crossroads",
				way_x = 41.4,
				way_y = 30.2,
				way_map = 1413
			},
			{
				faction = "Horde",
				name = "Omusa Thunderborn",
				location = "Camp Taurajo",
				way_x = 44.4,
				way_y = 59.0,
				way_map = 1413
			},
			{
				name = "Bragok",
				location = "Ratchet",
				way_x = 63.0,
				way_y = 37.2,
				way_map = 1413
			}
		}
	},
	{
		name = "Silverpine Forest",
		minLevel = 10,
		maxLevel = 20,
		faction = "Horde",
		continent = 1,
		flightMasters = {
			{
				faction = "Horde",
				name = "Karos Razok",
				location = "The Sepulcher",
				way_x = 45.6,
				way_y = 42.6,
				way_map = 1421
			}
		}
	},
	{
		name = "Westfall",
		minLevel = 10,
		maxLevel = 20,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Thor",
				location = "Sentinel Hill",
				way_x = 56.6,
				way_y = 52.6,
				way_map = 1436
			}
		}
	},
	{
		name = "Loch Modan",
		minLevel = 10,
		maxLevel = 20,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Thorgrum Borrelson",
				location = "Thelsamar",
				way_x = 33.8,
				way_y = 50.8,
				way_map = 1432
			}
		}
	},
	{
		name = "Darkshore",
		minLevel = 10,
		maxLevel = 20,
		faction = "Alliance",
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Caylais Moonfeather",
				location = "Auberdine",
				way_x = 36.4,
				way_y = 45.6,
				way_map = 1439
			}
		}
	},
	{
		name = "Redridge Mountains",
		minLevel = 15,
		maxLevel = 27,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Ariena Stormfeather",
				location = "Lakeshire",
				way_x = 30.6,
				way_y = 59.4,
				way_map = 1433
			}
		}
	},
	{
		name = "Stonetalon Mountains",
		minLevel = 15,
		maxLevel = 27,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Teloren",
				location = "Stonetalon Peak",
				way_x = 36.4,
				way_y = 7.2,
				way_map = 1442
			},
			{
				faction = "Horde",
				name = "Tharm",
				location = "Sun Rock Retreat",
				way_x = 45.2,
				way_y = 59.8,
				way_map = 1442
			}
		}
	},
	{
		name = "Ashenvale",
		minLevel = 18,
		maxLevel = 30,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Daelyshia",
				location = "Astranaar",
				way_x = 34.4,
				way_y = 48.0,
				way_map = 1440
			},
			{
				faction = "Horde",
				name = "Vhulgra",
				location = "Splintertree Post",
				way_x = 73.2,
				way_y = 61.6,
				way_map = 1440
			},
			{
				faction = "Horde",
				name = "Andruk",
				location = "Zoram'gar Outpost",
				way_x = 12.2,
				way_y = 33.8,
				way_map = 1440
			}
		}
	},
	{
		name = "Duskwood",
		minLevel = 18,
		maxLevel = 30,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Felicia Maline",
				location = "Darkshire",
				way_x = 77.6,
				way_y = 44.4,
				way_map = 1431
			}
		}
	},
	{
		name = "Hillsbrad Foothills",
		minLevel = 20,
		maxLevel = 30,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Darla Harris",
				location = "Southshore",
				way_x = 49.4,
				way_y = 52.2,
				way_map = 1424
			},
			{
				faction = "Horde",
				name = "Zarise",
				location = "Tarren Mill",
				way_x = 60.2,
				way_y = 18.6,
				way_map = 1424
			}
		}
	},
	{
		name = "Wetlands",
		minLevel = 20,
		maxLevel = 30,
		faction = "Alliance",
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Shellei Brondir",
				location = "Menethil Harbor",
				way_x = 9.6,
				way_y = 59.6,
				way_map = 1437
			}
		}
	},
	{
		name = "Thousand Needles",
		minLevel = 25,
		maxLevel = 35,
		continent = 2,
		flightMasters = {
			{
				faction = "Horde",
				name = "Nyse",
				location = "Freewind Post",
				way_x = 45.0,
				way_y = 49.2,
				way_map = 1441
			}
		}
	},
	{
		name = "Alterac Mountains",
		minLevel = 30,
		maxLevel = 40,
		continent = 1,
		flightMasters = {}
	},
	{
		name = "Arathi Highlands",
		minLevel = 30,
		maxLevel = 40,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Cedrik Prose",
				location = "Refuge Pointe",
				way_x = 45.6,
				way_y = 46.0,
				way_map = 1417
			},
			{
				faction = "Horde",
				name = "Urda",
				location = "Hammerfall",
				way_x = 73.0,
				way_y = 32.6,
				way_map = 1417
			}
		}
	},
	{
		name = "Desolace",
		minLevel = 30,
		maxLevel = 40,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Baritanas Skyriver",
				location = "Nijel's Point",
				way_x = 64.6,
				way_y = 10.4,
				way_map = 1443
			},
			{
				faction = "Horde",
				name = "Tal",
				location = "Shadowprey Village",
				way_x = 21.6,
				way_y = 74.0,
				way_map = 1443
			}
		}
	},
	{
		name = "Stranglethorn Vale",
		minLevel = 30,
		maxLevel = 45,
		continent = 1,
		flightMasters = {
			{
				faction = "Horde",
				name = "Thysta",
				location = "Grom'gol Base Camp",
				way_x = 32.6,
				way_y = 29.2,
				way_map = 1434
			},
			{
				faction = "Alliance",
				name = "Gyll",
				location = "Booty Bay",
				way_x = 27.6,
				way_y = 77.6,
				way_map = 1434
			},
			{
				faction = "Horde",
				name = "Gringer",
				location = "Booty Bay",
				way_x = 26.8,
				way_y = 77.0,
				way_map = 1434
			}
		}
	},
	{
		name = "Dustwallow Marsh",
		minLevel = 35,
		maxLevel = 45,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Baldruc",
				location = "Theramore Isle",
				way_x = 67.4,
				way_y = 51.2,
				way_map = 1445
			},
			{
				faction = "Horde",
				name = "Shardi",
				location = "Brackenwall Village",
				way_x = 35.6,
				way_y = 31.8,
				way_map = 1445
			}
		}
	},
	{
		name = "Badlands",
		minLevel = 35,
		maxLevel = 45,
		continent = 1,
		flightMasters = {
			{
				faction = "Horde",
				name = "Gorrik",
				location = "Kargath",
				way_x = 4.0,
				way_y = 44.8,
				way_map = 1418
			}
		}
	},
	{
		name = "Swamp of Sorrows",
		minLevel = 35,
		maxLevel = 45,
		continent = 1,
		flightMasters = {
			{
				faction = "Horde",
				name = "Breyk",
				location = "Stonard",
				way_x = 46.0,
				way_y = 54.6,
				way_map = 1435
			}
		}
	},
	{
		name = "Feralas",
		minLevel = 40,
		maxLevel = 50,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Thyssiana",
				location = "Lower Wilds",
				way_x = 89.4,
				way_y = 45.8,
				way_map = 1444
			},
			{
				faction = "Alliance",
				name = "Fyldren Moonfeather",
				location = "Feathermoon Stronghold",
				way_x = 30.2,
				way_y = 43.2,
				way_map = 1444
			},
			{
				faction = "Horde",
				name = "Shyn",
				location = "Camp Mojache",
				way_x = 75.4,
				way_y = 44.2,
				way_map = 1444
			}
		}
	},
	{
		name = "The Hinterlands",
		minLevel = 40,
		maxLevel = 50,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Guthrum Thunderfist",
				location = "Aerie Peak",
				way_x = 11.0,
				way_y = 46.0,
				way_map = 1425
			},
			{
				faction = "Horde",
				name = "Gorkas",
				location = "Revantusk Village",
				way_x = 81.6,
				way_y = 81.8,
				way_map = 1425
			}
		}
	},
	{
		name = "Tanaris",
		minLevel = 40,
		maxLevel = 50,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Bera Stonehammer",
				location = "Gadgetzan",
				way_x = 51.0,
				way_y = 29.2,
				way_map = 1446
			},
			{
				faction = "Horde",
				name = "Bulkrek Ragefist",
				location = "Gadgetzan",
				way_x = 51.6,
				way_y = 25.4,
				way_map = 1446
			}
		}
	},
	{
		name = "Searing Gorge",
		minLevel = 45,
		maxLevel = 50,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Lanie Reed",
				location = "Thorium Point",
				way_x = 37.8,
				way_y = 30.6,
				way_map = 1427
			},
			{
				faction = "Horde",
				name = "Grisha",
				location = "Thorium Point",
				way_x = 34.8,
				way_y = 30.8,
				way_map = 1427
			}
		}
	},
	{
		name = "Azshara",
		minLevel = 45,
		maxLevel = 55,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Jarrodenus",
				location = "asdasd",
				way_x = 11.8,
				way_y = 77.6,
				way_map = 1447
			},
			{
				faction = "Horde",
				name = "Tal",
				location = "asdasd",
				way_x = 22.0,
				way_y = 49.6,
				way_map = 1447
			}
		}
	},
	{
		name = "Blasted Lands",
		minLevel = 45,
		maxLevel = 55,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Alexandra Constantine",
				location = "Nethergarde Keep",
				way_x = 65.6,
				way_y = 24.4,
				way_map = 1419
			}
		}
	},
	{
		name = "Un'Goro Crater",
		minLevel = 48,
		maxLevel = 55,
		continent = 2,
		flightMasters = {
			{
				name = "Gryfe",
				location = "Marshal's Refuge",
				way_x = 45.2,
				way_y = 5.8,
				way_map = 1449
			}
		}
	},
	{
		name = "Felwood",
		minLevel = 48,
		maxLevel = 55,
		continent = 2,
		flightMasters = {
			{
				faction = "Horde",
				name = "Brakkar",
				location = "Bloodvenom Post",
				way_x = 34.4,
				way_y = 53.8,
				way_map = 1448
			}
		}
	},
	{
		name = "Burning Steppes",
		minLevel = 50,
		maxLevel = 58,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Borgus Stoutarm",
				location = "Morgan's Vigil",
				way_x = 84.4,
				way_y = 68.2,
				way_map = 1428
			},
			{
				faction = "Horde",
				name = "Vahgruk",
				location = "Flame Crest",
				way_x = 65.6,
				way_y = 24.2,
				way_map = 1428
			}
		}
	},
	{
		name = "Western Plaguelands",
		minLevel = 51,
		maxLevel = 58,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Bibilfaz Featherwhistle",
				location = "Chillwind Camp",
				way_x = 42.8,
				way_y = 85.0,
				way_map = 1422
			}
		}
	},
	{
		name = "Eastern Plaguelands",
		minLevel = 53,
		maxLevel = 60,
		continent = 1,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Khaelyn Steelwing",
				location = "Light's Hope Chapel",
				way_x = 81.6,
				way_y = 59.2,
				way_map = 1423
			},
			{
				faction = "Horde",
				name = "Georgia",
				location = "Light's Hope Chapel",
				way_x = 80.2,
				way_y = 57.0,
				way_map = 1423
			}
		}
	},
	{
		name = "Winterspring",
		minLevel = 53,
		maxLevel = 60,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Maethrya",
				location = "Everlook",
				way_x = 62.2,
				way_y = 36.6,
				way_map = 1452
			},
			{
				faction = "Horde",
				name = "Yugrek",
				location = "Everlook",
				way_x = 60.4,
				way_y = 36.4,
				way_map = 1452
			}
		}
	},
	{
		name = "Silithus",
		minLevel = 55,
		maxLevel = 60,
		continent = 2,
		flightMasters = {
			{
				faction = "Alliance",
				name = "Cloud Skydancer",
				location = "Cenarion Hold",
				way_x = 50.6,
				way_y = 34.6,
				way_map = 1451
			},
			{
				faction = "Horde",
				name = "Runk Windtamer",
				location = "Cenarion Hold",
				way_x = 48.8,
				way_y = 36.6,
				way_map = 1451
			}
		}
	}
}