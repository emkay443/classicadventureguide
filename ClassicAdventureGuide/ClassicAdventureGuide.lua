
-- load libraries
local ClassicAdventureGuide = _G["ClassicAdventureGuide"]
local AceGUI = LibStub("AceGUI-3.0")

local LDB = LibStub("LibDataBroker-1.1")
local dataobj = LDB:NewDataObject("ClassicAdventureGuide", {
	type = "data source",
	label = "ClassicAdventureGuide",
	text = "ClassicAdventureGuide",
	icon = "Interface\\AddOns\\ClassicAdventureGuide\\gfx\\icon.tga",
})

function ClassicAdventureGuide:IsOpen()
	return self.mainFrame and self.mainFrame:IsVisible()
end

------------------------------------------------------------------------------------------------
-- Data broker

function dataobj:OnEnter()
	GameTooltip:SetOwner(self, "ANCHOR_NONE")
	GameTooltip:SetPoint("TOPLEFT", self, "BOTTOMLEFT")
	GameTooltip:ClearLines()
	GameTooltip:AddLine("Classic Adventure Guide", 0, 1, 1)
	GameTooltip:AddLine("Left-Click to toggle Classic Adventure Guide", 0, 0.7, 1)

	GameTooltip:Show()
end

function dataobj:OnLeave()
	GameTooltip:Hide()
end

function dataobj:OnClick(button)
	if button == "LeftButton" then
		if not ClassicAdventureGuide:IsOpen() then
			ClassicAdventureGuide:CreateFrame()
		else
			ClassicAdventureGuide:closeWindow()
		end
	elseif button == "RightButton" then
			-- InterfaceOptionsFrame_OpenToCategory("ClassicAdventureGuide")
	end
end

local icon = LibStub("LibDBIcon-1.0")
ClassicAdventureGuide.db = LibStub("AceDB-3.0"):New("ClassicAdventureGuideDB", { profile = { minimap = { hide = false, }, }, })
icon:Register("ClassicAdventureGuideBtn", dataobj, ClassicAdventureGuide.db.profile.minimap)

SLASH_CLASSICADVENTUREGUIDE1 = "/cag"
SLASH_CLASSICADVENTUREGUIDE2 = "/classicadventureguide"
SlashCmdList["CLASSICADVENTUREGUIDE"] = function(msg)
    ClassicAdventureGuide:ToggleFrame()
end

------------------------------------------------------------------------------------------------
-- Custom functions

function ClassicAdventureGuide:DrawSuggestedContentGroup(container)
	local playerLevel = UnitLevel("player")
	local playerFaction = select(1, UnitFactionGroup("player"))

	container:ReleaseChildren()

	local scrollGroup = AceGUI:Create("InlineGroup")
	scrollGroup:SetTitle("Suggested Content")
	scrollGroup:SetFullWidth(true)
	scrollGroup:SetFullHeight(true)
	scrollGroup:SetLayout("Fill")

	local scrollFrame = AceGUI:Create("ScrollFrame")
	scrollFrame:SetLayout("Flow")

	if playerLevel >= 60 then
		local lab = AceGUI:Create("Label")
		lab:SetText("|cFFFFFF00-- RAIDS --|r")
		lab:SetFullWidth(true)
		scrollFrame:AddChild(lab)

		local raidLockouts = {}
		for i=1,GetNumSavedInstances() do
			local lockoutName, _, lockoutTime = GetSavedInstanceInfo(i)
			table.insert(raidLockouts, { name = lockoutName, time = lockoutTime })
		end

		for _, raid in ipairs(CAG_Raids) do
			local raidIsLockedOut = false
			for _, lockout in ipairs(raidLockouts) do
				if (raid['name'] == lockout['name']) or (lockout['name'] == "Ahn'Qiraj Temple" and raid['name'] == "Temple of Ahn'Qiraj") then
					raidIsLockedOut = true
					break
				end
			end

			if (not raidIsLockedOut) and raid['releasePhase'] == nil then
				local newRaid = AceGUI:Create("InteractiveLabel")
				newRaid:SetText('|cFF639FFF' .. raid['name'] .. '|r (|cFF00FF00' .. raid['numPlayers'] .. ' players|r in |cFFFFFF00' .. raid['zone'] .. '|r)')
				newRaid:SetFullWidth(true)
				newRaid:SetCallback("OnClick", function() CAG_SelectDungeonSrc = 'suggestedcontent'; ClassicAdventureGuide:SelectDungeon(container, raid, 1) end)
				scrollFrame:AddChild(newRaid)
			end
		end

		local function getResetTime(t)
			local days = math.floor(t / 86400)
			t = t - days * 86400
			local hours = math.floor(t / 3600)
			t = t - hours * 3600
			local minutes = math.floor(t / 60)

			return days .. 'd ' .. hours .. 'h ' .. minutes .. 'm'
		end

		for _, raid in ipairs(CAG_Raids) do
			local raidIsLockedOut = false
			for _, lockout in ipairs(raidLockouts) do
				if (raid['name'] == lockout['name']) or (lockout['name'] == "Ahn'Qiraj Temple" and raid['name'] == "Temple of Ahn'Qiraj") then
					raidIsLockedOut = true
					raid['lockoutTime'] = lockout['time']
					break
				end
			end

			if raidIsLockedOut then
				local newRaid = AceGUI:Create("InteractiveLabel")
				newRaid:SetText('|cFFA3A3A3' .. raid['name'] .. " (resets in " .. getResetTime(raid['lockoutTime']) .. ')|r')
				newRaid:SetFullWidth(true)
				newRaid:SetCallback("OnClick", function() CAG_SelectDungeonSrc = 'suggestedcontent'; ClassicAdventureGuide:SelectDungeon(container, raid, 1) end)
				scrollFrame:AddChild(newRaid)
			end
		end

		for _, raid in ipairs(CAG_Raids) do
			if raid["releasePhase"] ~= nil then
				local newRaid = AceGUI:Create("InteractiveLabel")
				newRaid:SetText('|cFF5E5E5E' .. raid['name'] .. " (will be released in Phase " .. raid["releasePhase"] .. ')|r')
				newRaid:SetFullWidth(true)
				newRaid:SetCallback("OnClick", function() CAG_SelectDungeonSrc = 'suggestedcontent'; ClassicAdventureGuide:SelectDungeon(container, raid, 1) end)
				scrollFrame:AddChild(newRaid)
			end
		end

		local lab = AceGUI:Create("Label")
		lab:SetText("\n|cFFFFFF00-- WORLD BOSSES --|r")
		lab:SetFullWidth(true)
		scrollFrame:AddChild(lab)

		for _, worldboss in ipairs(CAG_Worldbosses) do
			local newWB = AceGUI:Create("InteractiveLabel")
			newWB:SetText('|cFF639FFF' .. worldboss['name'] .. '|r (|cFF00FF00' .. worldboss['numPlayers'] .. ' players|r in |cFFFFFF00' .. worldboss['zone'] .. '|r)')
			newWB:SetFullWidth(true)
			newWB:SetCallback("OnClick", function() CAG_SelectDungeonSrc = 'suggestedcontent'; ClassicAdventureGuide:SelectDungeon(container, worldboss, 2) end)
			scrollFrame:AddChild(newWB)
		end

		local lab = AceGUI:Create("Label")
		lab:SetText("\n")
		lab:SetFullWidth(true)
		scrollFrame:AddChild(lab)
	else
		local lab = AceGUI:Create("Label")
		lab:SetText("|cFFFFFF00-- QUESTING ZONES --|r")
		lab:SetFullWidth(true)
		scrollFrame:AddChild(lab)

		if ClassicAdventureGuide:TomTomIntegrationIsEnabled() then
			local lab = AceGUI:Create("Label")
			lab:SetText("Click on a flight master to add a TomTom waypoint.")
			lab:SetFullWidth(true)
			scrollFrame:AddChild(lab)
		end

		local lab = AceGUI:Create("Label")
		lab:SetText("\n")
		lab:SetFullWidth(true)
		scrollFrame:AddChild(lab)

		local function GetContinentName(id)
			if id == 1 then
				return "Eastern Kingdoms"
			else
				return "Kalimdor"
			end
		end

		for _, questingZone in ipairs(CAG_QuestingZones) do
			if playerLevel >= questingZone['minLevel'] and playerLevel <= questingZone['maxLevel'] and (questingZone['faction'] == nil or questingZone['faction'] == playerFaction) then
				local newQuestingZone = AceGUI:Create("InteractiveLabel")
				newQuestingZone:SetText('|cFF639FFF' .. questingZone['name'] .. '|r (|cFF00FF00' .. questingZone['minLevel'] .. '-' .. questingZone['maxLevel'] .. '|r in |cFFFFFF00' .. GetContinentName(questingZone['continent']) .. '|r)')
				newQuestingZone:SetFullWidth(true)
				scrollFrame:AddChild(newQuestingZone)

				if table.getn(questingZone['flightMasters']) > 0 then
					local flightMasters = {}
					for _, flightMaster in ipairs(questingZone['flightMasters']) do
						if flightMaster['faction'] == nil or playerFaction == flightMaster['faction'] then
							table.insert(flightMasters, flightMaster)
						end
					end

					if table.getn(flightMasters) > 0 then
						local lab = AceGUI:Create("Label")
						lab:SetText('Flight masters:')
						lab:SetFullWidth(true)
						scrollFrame:AddChild(lab)

						for _, flightMaster in ipairs(flightMasters) do
							local newFlightMaster = AceGUI:Create("InteractiveLabel")
							local factionString = ""
							local flightMasterColor = "|cFFFFFF00"

							if flightMaster['faction'] ~= nil then
								factionString = '@' .. string.lower(flightMaster['faction']) .. ' '
								if flightMaster['faction'] == "Horde" then
									flightMasterColor = "|cFFFF0000"
								else
									flightMasterColor = "|cFF5A93CC"
								end
							end

							local flightMasterString = "- " .. factionString .. flightMasterColor .. flightMaster['name'] .. "|r"
							if flightMaster['location'] ~= nil then
								flightMasterString = flightMasterString .. " in " .. flightMasterColor .. flightMaster['location'] .. "|r"
							end

							if ClassicAdventureGuide:TomTomIntegrationIsEnabled() then
								newFlightMaster:SetCallback("OnClick", function()
									ClassicAdventureGuide:TomTomIntegrationAddWaypoint(flightMaster['way_map'], flightMaster['way_x'], flightMaster['way_y'], ClassicAdventureGuide:GetIconString("Flight master " .. factionString .. flightMasterColor .. flightMaster['name'] .. "|r"))
								end)
							end

							newFlightMaster:SetText(ClassicAdventureGuide:GetIconString(flightMasterString))
							newFlightMaster:SetFullWidth(true)
							scrollFrame:AddChild(newFlightMaster)
						end
					end
				end

				local lab = AceGUI:Create("Label")
				lab:SetText('\n')
				lab:SetFullWidth(true)
				scrollFrame:AddChild(lab)
			end
		end

	end

	local suggestedDungeons = {}
	for _, dungeon in ipairs(CAG_Dungeons) do
		if playerLevel + 3 >= dungeon['minLevel'] and playerLevel - 3 <= dungeon['maxLevel'] and (dungeon['faction'] == nil or dungeon['faction'] == playerFaction) then
			table.insert(suggestedDungeons, dungeon)
		end
	end

	if table.getn(suggestedDungeons) > 0 then
		local lab = AceGUI:Create("Label")
		lab:SetText("|cFFFFFF00-- DUNGEONS --|r")
		lab:SetFullWidth(true)
		scrollFrame:AddChild(lab)

		for _, dungeon in ipairs(suggestedDungeons) do
			local newDungeon = AceGUI:Create("InteractiveLabel")
			newDungeon:SetText('|cFF639FFF' .. dungeon['name'] .. '|r (|cFF00FF00' .. dungeon['minLevel'] .. '-' .. dungeon['maxLevel'] .. '|r in |cFFFFFF00' .. dungeon['zone'] .. '|r)')
			newDungeon:SetFullWidth(true)
			newDungeon:SetCallback("OnClick", function() CAG_SelectDungeonSrc = 'suggestedcontent'; ClassicAdventureGuide:SelectDungeon(container, dungeon, 0) end)
			scrollFrame:AddChild(newDungeon)
		end
	end

	scrollGroup:AddChild(scrollFrame)
	container:AddChild(scrollGroup)
end

function ClassicAdventureGuide:SelectEncounter(container, npc_id, dungeon)
	container:ReleaseChildren()

	local scrollGroup = AceGUI:Create("InlineGroup")
	scrollGroup:SetFullWidth(true)
	scrollGroup:SetFullHeight(true)
	scrollGroup:SetLayout("Fill")

	local scrollFrame = AceGUI:Create("ScrollFrame")
	scrollFrame:SetLayout("Flow")

	local encounterTable = _G['CAG_Encounters_' .. dungeon["short"]]

	if encounterTable ~= nil then
		local encounterFound = false
		local foundEncounter
		for _, encounter in ipairs(encounterTable) do
			if encounter['npc_id'] == npc_id then
				encounterFound = true
				foundEncounter = encounter
				break
			end
		end

		local indexOffset = 0
		if encounterFound then
			if foundEncounter['description'] ~= nil then
				local lab = AceGUI:Create("Label")
				lab:SetFullWidth(true)
				lab:SetText(foundEncounter['description'])
				scrollFrame:AddChild(lab)

				lab = AceGUI:Create("Label")
				lab:SetFullWidth(true)
				lab:SetText('\n')
				scrollFrame:AddChild(lab)
			end

			if table.getn(foundEncounter['phases']) > 1 then
				for index, phase in ipairs(foundEncounter['phases']) do
					local phaseString

					if phase.preparation then
						phaseString = "|cFFFFFF00-- PREPARATION --|r"
						indexOffset = indexOffset + 1
					else
						phaseString = string.format("|cFFFFFF00-- PHASE %d --|r", index - indexOffset)
					end

					local lab = AceGUI:Create("Label")
					lab:SetFullWidth(true)
					lab:SetText(phaseString)
					scrollFrame:AddChild(lab)

					if phase['abilities'] ~= nil and table.getn(phase['abilities']) > 0 then
						lab = AceGUI:Create("Label")
						lab:SetFullWidth(true)
						lab:SetText("|cFFFFFF00Abilities:|r")
						scrollFrame:AddChild(lab)

						for _, ability in ipairs(phase['abilities']) do
							local spellName = select(1, GetSpellLink(ability))
							local spellIcon = select(3, GetSpellInfo(ability))
							lab = AceGUI:Create("InteractiveLabel")
							lab:SetFullWidth(true)
							lab:SetText(string.format("|cFF71d5FF|Hspell:%d|h[%s]|h|r", ability, spellName))
							lab:SetImage(spellIcon)
							lab:SetCallback("OnClick", function() SetItemRef("spell:" .. ability, spell) end)
							scrollFrame:AddChild(lab)
						end

						lab = AceGUI:Create("Label")
						lab:SetFullWidth(true)
						lab:SetText("\n")
						scrollFrame:AddChild(lab)
					end

					lab = AceGUI:Create("Label")
					lab:SetFullWidth(true)
					lab:SetText("|cFFFFFF00Strategy:|r")
					scrollFrame:AddChild(lab)

					for _, strategy in ipairs(phase['strategy']) do
						lab = AceGUI:Create("InteractiveLabel")
						lab:SetFullWidth(true)
						lab:SetText(ClassicAdventureGuide:GetIconString(strategy))
						scrollFrame:AddChild(lab)
					end

					lab = AceGUI:Create("Label")
					lab:SetFullWidth(true)
					lab:SetText("\n")
					scrollFrame:AddChild(lab)
				end
			else
				local phase = foundEncounter['phases'][1]

				if phase['abilities'] ~= nil and table.getn(phase['abilities']) > 0 then
					lab = AceGUI:Create("Label")
					lab:SetFullWidth(true)
					lab:SetText("|cFFFFFF00Abilities:|r")
					scrollFrame:AddChild(lab)

					for _, ability in ipairs(phase['abilities']) do
						local spellName = select(1, GetSpellLink(ability))
						local spellIcon = select(3, GetSpellInfo(ability))
						lab = AceGUI:Create("InteractiveLabel")
						lab:SetFullWidth(true)
						lab:SetText(string.format("|cFF71d5FF|Hspell:%d|h[%s]|h|r", ability, spellName))
						lab:SetImage(spellIcon)
						lab:SetCallback("OnClick", function() SetItemRef("spell:" .. ability, spell) end)
						scrollFrame:AddChild(lab)
					end

					lab = AceGUI:Create("Label")
					lab:SetFullWidth(true)
					lab:SetText("\n")
					scrollFrame:AddChild(lab)
				end

				lab = AceGUI:Create("Label")
				lab:SetFullWidth(true)
				lab:SetText("|cFFFFFF00Strategy:|r")
				scrollFrame:AddChild(lab)

				for _, strategy in ipairs(phase['strategy']) do
					lab = AceGUI:Create("InteractiveLabel")
					lab:SetFullWidth(true)
					lab:SetText(ClassicAdventureGuide:GetIconString(strategy))
					scrollFrame:AddChild(lab)
				end

				lab = AceGUI:Create("Label")
				lab:SetFullWidth(true)
				lab:SetText("\n")
				scrollFrame:AddChild(lab)
			end

			if ClassicAdventureGuide:AtlasLootIntegrationIsEnabled() then
				local alBtn = AceGUI:Create("Button")
				alBtn:SetText("Loot")
				alBtn:SetWidth(100)
				alBtn:SetCallback("OnClick", function() ClassicAdventureGuide:AtlasLootIntegrationShowLoot(dungeon, foundEncounter) end)
				scrollFrame:AddChild(alBtn)
			end
		end
	end

	scrollGroup:AddChild(scrollFrame)
	container:AddChild(scrollGroup)
end

CAG_SelectDungeonSrc = 'dungeons'
function ClassicAdventureGuide:SelectDungeon(container, dungeon, dungeonType)
	local frameTitle = "Dungeon"

	if dungeonType == 1 then
		frameTitle = "Raid"
	elseif dungeonType == 2 then
		frameTitle = "World Boss"
	end
	container:ReleaseChildren()

	local lab = AceGUI:Create("Label")
	lab:SetText("Selected " .. frameTitle .. ": |cFFFFFF00" .. dungeon["name"] .. "|r")
	lab:SetFullWidth(true)
	container:AddChild(lab)

	local backBtn = AceGUI:Create("Button")
	backBtn:SetText("Go Back")
	backBtn:SetWidth(100)
	backBtn:SetCallback("OnClick", function()
		if CAG_SelectDungeonSrc == 'dungeons' then
			ClassicAdventureGuide:DrawDungeonsGroup(container, dungeonType)
		else
			ClassicAdventureGuide:DrawSuggestedContentGroup(container)
		end
	end)
	container:AddChild(backBtn)

	if ClassicAdventureGuide:AtlasIntegrationIsEnabled() and dungeon["mapId"] ~= nil then
		local atlasBtn = AceGUI:Create("Button")
		atlasBtn:SetText("Map")
		atlasBtn:SetWidth(100)
		atlasBtn:SetCallback("OnClick", function() ClassicAdventureGuide:AtlasIntegrationShowMap(dungeon["mapId"]) end)
		container:AddChild(atlasBtn)

		if dungeon["mapIdEntrance"] ~= nil then
			local atlasEntBtn = AceGUI:Create("Button")
			atlasEntBtn:SetText("Map (Entrance)")
			atlasEntBtn:SetWidth(150)
			atlasEntBtn:SetCallback("OnClick", function() ClassicAdventureGuide:AtlasIntegrationShowMap(dungeon["mapIdEntrance"]) end)
			container:AddChild(atlasEntBtn)
		end
	end

	if ClassicAdventureGuide:TomTomIntegrationIsEnabled() and dungeon["way_x"] ~= nil then
		local atlasBtn = AceGUI:Create("Button")
		atlasBtn:SetText("Waypoint")
		atlasBtn:SetWidth(100)
		atlasBtn:SetCallback("OnClick", function() ClassicAdventureGuide:TomTomIntegrationAddWaypoint(dungeon['way_map'], dungeon['way_x'], dungeon['way_y'], dungeon['name']) end)
		container:AddChild(atlasBtn)
	end

	local encounterTabs = AceGUI:Create("TabGroup")
	encounterTabs:SetFullWidth(true)
	encounterTabs:SetFullHeight(true)
	encounterTabs:SetLayout("Flow")

	local encounterTable = _G['CAG_Encounters_' .. dungeon["short"]]
	if encounterTable ~= nil then
		local tabs = {}
		local firstEncounter
		for index, encounter in ipairs(encounterTable) do
			if firstEncounter == nil then firstEncounter = encounter['npc_id'] end
			table.insert(tabs, { text = encounter['name'], value = encounter['npc_id'] })
		end
		encounterTabs:SetTabs(tabs)
		encounterTabs:SetCallback("OnGroupSelected", function (container, event, npc_id) ClassicAdventureGuide:SelectEncounter(container, npc_id, dungeon) end)
		encounterTabs:SelectTab(firstEncounter)
	end

	container:AddChild(encounterTabs)
end

function ClassicAdventureGuide:DrawDungeonsGroup(container, dungeonType)
	local frameTitle = "Dungeons"
	local dungeonsTable = CAG_Dungeons

	if dungeonType == 1 then
		frameTitle = "Raids"
		dungeonsTable = CAG_Raids
	elseif dungeonType == 2 then
		frameTitle = "World Bosses"
		dungeonsTable = CAG_Worldbosses
	end

	container:ReleaseChildren()

	local scrollGroup = AceGUI:Create("InlineGroup")
	scrollGroup:SetTitle(frameTitle)
	scrollGroup:SetFullWidth(true)
	scrollGroup:SetFullHeight(true)
	scrollGroup:SetLayout("Fill")

	local scrollFrame = AceGUI:Create("ScrollFrame")
	scrollFrame:SetLayout("Flow")

	for index, dungeon in ipairs(dungeonsTable) do
		local dungeonButton = AceGUI:Create("Button")
		dungeonButton:SetWidth(120)
		dungeonButton:SetHeight(80)
		dungeonButton:SetText(dungeon["name"])
		dungeonButton:SetCallback("OnClick", function() CAG_SelectDungeonSrc = "dungeons"; ClassicAdventureGuide:SelectDungeon(container, dungeon, dungeonType) end)
		scrollFrame:AddChild(dungeonButton)
	end

	scrollGroup:AddChild(scrollFrame)
	container:AddChild(scrollGroup)
end

function ClassicAdventureGuide:SelectGroup(container, group)
	if group == "suggestedcontent" then
		ClassicAdventureGuide:DrawSuggestedContentGroup(container)
	elseif group == "dungeons" then
		ClassicAdventureGuide:DrawDungeonsGroup(container, 0)
	elseif group == "raids" then
		ClassicAdventureGuide:DrawDungeonsGroup(container, 1)
	elseif group == "worldbosses" then
		ClassicAdventureGuide:DrawDungeonsGroup(container, 2)
	elseif group == "reload" then
		ReloadUI()
	end
end

function ClassicAdventureGuide:CreateFrame()
	self.mainFrame = AceGUI:Create("Frame")
	self.mainFrame:SetTitle("Classic Adventure Guide")
	self.mainFrame:SetStatusText("Proudly developed by |cFFFF7D0AWildmane <Outer Rim>|r and |cFF69CCF0Clockwerk <Outer Rim>|r on |TInterface\\Addons\\ClassicAdventureGuide\\gfx\\horde:16|t |cFFFF0000Venoxis (EU)|r")
	self.mainFrame:SetCallback("OnClose", function() self.closeWindow() end)
	self.mainFrame:SetLayout("Fill")
	self.mainFrame:SetWidth(800)
	self.mainFrame:SetHeight(550)

	self.tabGroup = AceGUI:Create("TabGroup")
	self.tabGroup:SetLayout("Flow")
	self.tabGroup:SetTabs({
		{
			text="Suggested Content",
			value="suggestedcontent"
		},
		{
			text="Dungeons",
			value="dungeons"
		},
		{
			text="Raids",
			value="raids"
		},
		{
			text="World Bosses",
			value="worldbosses"
		},
		{
			text="RELOAD",
			value="reload"
		}
	})
	self.tabGroup:SetCallback("OnGroupSelected", function(container, event, group) ClassicAdventureGuide:SelectGroup(container, group) end)
	self.tabGroup:SelectTab("suggestedcontent")

	self.mainFrame:AddChild(self.tabGroup)

	self.mainFrame:Show()

	_G["ClassicAdventureGuideFrame"] = self.mainFrame.frame
	tinsert(UISpecialFrames, "ClassicAdventureGuideFrame")
end


function ClassicAdventureGuide:closeWindow()
	AceGUI:Release(ClassicAdventureGuide.mainFrame)
end

function ClassicAdventureGuide:ToggleFrame()
	if not ClassicAdventureGuide:IsOpen() then
		ClassicAdventureGuide:CreateFrame()
	else
		ClassicAdventureGuide:closeWindow()
	end
end