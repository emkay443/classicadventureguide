local ClassicAdventureGuide = _G["ClassicAdventureGuide"]
print("A")

function GetMapId(s)
	print(TomTom.NameToMapId[s])
end

function ClassicAdventureGuide:AtlasIntegrationIsEnabled()
	local loadable = select(4, GetAddOnInfo("Atlas"))
	local enabled = GetAddOnEnableState(UnitName("player"), "Atlas")

	if (enabled > 0 and loadable) then
		return true
	else
		return false
	end
end

function ClassicAdventureGuide:AtlasLootIntegrationIsEnabled()
	local loadable = select(4, GetAddOnInfo("AtlasLootClassic"))
	local enabled = GetAddOnEnableState(UnitName("player"), "AtlasLootClassic")

	if (enabled > 0 and loadable) then
		return true
	else
		return false
	end
end

function ClassicAdventureGuide:TomTomIntegrationIsEnabled()
	local loadable = select(4, GetAddOnInfo("TomTom"))
	local enabled = GetAddOnEnableState(UnitName("player"), "TomTom")

	if (enabled > 0 and loadable) then
		return true
	else
		return false
	end
end

function ClassicAdventureGuide:AtlasIntegrationGetAtlasZoneData(mapID, setMap)
	if (not ClassicAdventureGuide:AtlasIntegrationIsEnabled()) then return end
	local options = Atlas.db.profile.options
	local foundMatch = false
	for k, v in pairs(ATLAS_DROPDOWNS) do
		for k2, v2 in pairs(v) do
			if (v2 == mapID) then
				if setMap then
					options.dropdowns.module = k
					options.dropdowns.zone = k2
				end
				foundMatch = true
				break
			end
		end
		if foundMatch then break end
	end

	return foundMatch
end

function ClassicAdventureGuide:AtlasIntegrationShowMap(mapID)
	if (not ClassicAdventureGuide:AtlasIntegrationIsEnabled()) then return end

	ATLAS_SMALLFRAME_SELECTED_ORIG = ATLAS_SMALLFRAME_SELECTED
	ATLAS_SMALLFRAME_SELECTED = true

	ClassicAdventureGuide:AtlasIntegrationGetAtlasZoneData(mapID, true)

	if ( AtlasFrameLarge:IsVisible() ) then
		HideUIPanel(AtlasFrameLarge)
	end
	if ( AtlasFrame:IsVisible() ) then
		HideUIPanel(AtlasFrame)
	end
	if (not AtlasFrameSmall:IsVisible() ) then
		ShowUIPanel(AtlasFrameSmall)
	end
	AtlasFrameDropDownType_OnShow()
	AtlasFrameDropDown_OnShow()
	Atlas_Refresh()

	ATLAS_SMALLFRAME_SELECTED = ATLAS_SMALLFRAME_SELECTED_ORIG
end

function ClassicAdventureGuide:AtlasLootIntegrationShowLoot(dungeon, encounter)
	local db = AtlasLoot.db.GUI;

	local ALModule = "AtlasLootClassic_DungeonsAndRaids";
	local bossID = nil;

	if (not AtlasLoot.GUI.frame:IsVisible()) then
		AtlasLoot.GUI.frame:Show();
	end
	-- Set module
	if (ALModule ~= db.selected[1]) then
		AtlasLoot.GUI.frame.moduleSelect:SetSelected(ALModule); -- this should also force AtlasLoot to load the module data
	end

	local moduleData = AtlasLoot.ItemDB:Get(ALModule);
	local dataID;
	-- to search the right instance
	for k, v in pairs(moduleData) do
		if (type(v) == "table") then
			for ka, va in pairs(v) do
				if ka == "AtlasMapFile" then
					if type(va) == "table" then
						for kb, vb in pairs(va) do
							if vb == string.sub(dungeon["mapId"], 4) then
								dataID = k;
								break;
							end
						end
						if (dataID) then break; end
					else
						if va == string.sub(dungeon["mapId"], 4) then
							dataID = k;
							break;
						end
					end
				end
			end
		end
		if (dataID) then break; end
	end
	dataID = dataID or db.selected[2];
	-- Set sub-category (instance)
	if (dataID ~= db.selected[2]) then
		AtlasLoot.GUI.frame.subCatSelect:SetSelected(dataID);
	end

	for count = 1, #moduleData[dataID].items do
		if (moduleData[dataID].items[count].npcID == encounter["npc_id"]) then
			bossID = count;
			break;
		end
	end
	-- Set boss selection
	if (bossID and bossID ~= db.selected[3]) then
		AtlasLoot.GUI.frame.boss:SetSelected(bossID)
	end
--[[
	local difficulties = moduleData:GetDifficultys()
	local difficultyID;
	-- look for the 1st difficulty
	for count = 1, #difficulties do
		if moduleData[dataID].items[1][count] then
			difficultyID = count;
			break;
		end
	end
	-- Set difficulty
	--AtlasLoot.GUI.frame.difficulty:SetSelected(difficultyID)
]]
	AtlasLoot.GUI.ItemFrame:Refresh(true);
end

function ClassicAdventureGuide:TomTomIntegrationAddWaypoint(way_map, way_x, way_y, way_title)
	TomTom:AddWaypoint(
		way_map,
		way_x / 100,
		way_y / 100,
		{
			title = way_title,
			from = "ClassicAdventureGuide"
		}
	)
end

function ClassicAdventureGuide:GetIconString(s)
	local baseTextureString = "|TInterface\\Addons\\ClassicAdventureGuide\\gfx\\%s:16|t"
	local returnString = s

	returnString, _ = string.gsub(returnString, "@tank", string.format(baseTextureString, "role_tank"))
	returnString, _ = string.gsub(returnString, "@heal", string.format(baseTextureString, "role_heal"))
	returnString, _ = string.gsub(returnString, "@melee", string.format(baseTextureString, "role_melee"))
	returnString, _ = string.gsub(returnString, "@dps", string.format(baseTextureString, "role_dps"))
	returnString, _ = string.gsub(returnString, "@ranged", string.format(baseTextureString, "role_ranged"))

	returnString, _ = string.gsub(returnString, "@hordetext", string.format(baseTextureString, "horde") .. " |cFFFF0000Horde|r")
	returnString, _ = string.gsub(returnString, "@alliancetext", string.format(baseTextureString, "alliance") .. " |cFF5A93CCAlliance|r")

	returnString, _ = string.gsub(returnString, "@horde", string.format(baseTextureString, "horde"))
	returnString, _ = string.gsub(returnString, "@alliance", string.format(baseTextureString, "alliance"))

	returnString, _ = string.gsub(returnString, "@druidtext", string.format(baseTextureString, "class_druid") .. " |cFFFF7D0ADruid|r")
	returnString, _ = string.gsub(returnString, "@huntertext", string.format(baseTextureString, "class_hunter") .. " |cFFABD473Hunter|r")
	returnString, _ = string.gsub(returnString, "@magetext", string.format(baseTextureString, "class_mage") .. " |cFF69CCF0Mage|r")
	returnString, _ = string.gsub(returnString, "@paladintext", string.format(baseTextureString, "class_paladin") .. " |cFFF58CBAPaladin|r")
	returnString, _ = string.gsub(returnString, "@priesttext", string.format(baseTextureString, "class_priest") .. " |cFFFFFFFFPriest|r")
	returnString, _ = string.gsub(returnString, "@roguetext", string.format(baseTextureString, "class_rogue") .. " |cFFFFF569Rogue|r")
	returnString, _ = string.gsub(returnString, "@shamantext", string.format(baseTextureString, "class_shaman") .. " |cFF0070DEShaman|r")
	returnString, _ = string.gsub(returnString, "@warlocktext", string.format(baseTextureString, "class_warlock") .. " |cFF9482C9Warlock|r")
	returnString, _ = string.gsub(returnString, "@warriortext", string.format(baseTextureString, "class_warrior") .. " |cFFC79C6EWarrior|r")

	returnString, _ = string.gsub(returnString, "@druid", string.format(baseTextureString, "class_druid"))
	returnString, _ = string.gsub(returnString, "@hunter", string.format(baseTextureString, "class_hunter"))
	returnString, _ = string.gsub(returnString, "@mage", string.format(baseTextureString, "class_mage"))
	returnString, _ = string.gsub(returnString, "@paladin", string.format(baseTextureString, "class_paladin"))
	returnString, _ = string.gsub(returnString, "@priest", string.format(baseTextureString, "class_priest"))
	returnString, _ = string.gsub(returnString, "@rogue", string.format(baseTextureString, "class_rogue"))
	returnString, _ = string.gsub(returnString, "@shaman", string.format(baseTextureString, "class_shaman"))
	returnString, _ = string.gsub(returnString, "@warlock", string.format(baseTextureString, "class_warlock"))
	returnString, _ = string.gsub(returnString, "@warrior", string.format(baseTextureString, "class_warrior"))

	returnString, _ = string.gsub(returnString, "@spell%[(%d+)%]%[([a-zA-Z0-9'%(%) ]+)%]", "|cFF71d5FF|Hspell:%1|h[%2]|h|r")

	return returnString
end